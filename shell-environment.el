;;; shell-environment.el -- Shell environment to emacs -*- lexical-binding:t -*-

;; Copyright (C) 2023  Alejandro Barocio Alvarez

;; Author: Alejandro Barocio A. <alejandro@barocio.cc>
;; Maintainer: Alejandro Barocio A. <alejandro@barocio.cc>
;; URL: https://codeberg.org/barocio/shell-environment.el
;; Version: 0.1.1
;; Package-Requires: ((emacs "27.1"))
;; Keywords: comm

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:

;; This code is meant to import the actual values of the shell
;; environment (BASH, KSH, ASH, DASH, etc.) into your Emacs
;; environment.

;; Sometimes (depending on how you started your emacs process/daemon)
;; the shell environment variables don't get inherited as expected.
;; This could cause multiple problems, from simple warnings about a
;; non existent directory, to being unable to make push/pull with
;; magit (my specific case).  This is menant to correct those
;; situations with elegance.

;;; Code:


;;; Code

;;;; Variables

(defgroup shell-environment nil
  "Shell environment importation customizations."
  :group 'applications)

(defcustom shell-environment-process-name "*shell-environment*"
  "Tempalte to use to create the process name and the output buffer name."
  :group 'shell-environment
  :type 'string)

(defcustom shell-environment-shell-binary "sh"
  "Name of the shell to use (the basename)."
  :group 'shell-environment
  :type 'string)

(defcustom shell-environment-keep-buffer nil
  "Wether keep the process buffer or kill it on finish."
  :group 'shell-environment
  :type 'boolean)

(defvar shell-environment--alist '()
  "Association list with pairs of NAME/VALUE for shell variables.")


;;;; Functions
;;;###autoload
(defun shell-environment-init ()
  "Initializes the variable `shell-environment--alist' with it's values."
  (or shell-environment-keep-buffer
      (and  (get-buffer shell-environment-process-name)
	    (kill-buffer (get-buffer shell-environment-process-name))))
  (let ((process
	 (start-process shell-environment-process-name
			(get-buffer-create shell-environment-process-name)
			(executable-find shell-environment-shell-binary)
			"-i" "-c"
			"env | grep -ve 'PS[0-9]=' | sed 's#=#@:!:@#' | awk -F'@:!:@' 'BEGIN{printf(\";;; -*- lisp-data -*-\\n(\");} END{printf(\")\");} {printf(\"(\\\"%s\\\" . \\\"%s\\\")\", $1, $2)}'")))
    (while (eql 'run (process-status process))
      (sleep-for 0.001))
    (sleep-for 0.001)
    (if (eq (process-exit-status process) 0)
	(with-current-buffer (get-buffer shell-environment-process-name)
	  (goto-char (point-min))
	  (setq shell-environment--alist (read (current-buffer))))
      (warn "Procces exit status was: %s"
	    (process-exit-status process)))
    ;; (or shell-environment-keep-buffer
	;; (kill-buffer (get-buffer shell-environment-process-name)))
    )
  shell-environment--alist)

;;;###autoload
(defun shell-environment-getenv (var)
  "Extract the value of a variable from `shell-environment--alist'.

VAR here could be any symbol or string."
  (let ((var-name
	 (if (stringp var)
	     var
	   (if (symbolp var)
	       (replace-regexp-in-string "^:" ""
					 (symbol-name var))
	     (error "Parameter must be a STRING or a SYMBOL.")))))
    (cdr (assoc var-name shell-environment--alist))))

;;;###autoload
(defun shell-environment-import-variable (var)
  "Imports/sets environment variable VAR according to it's value on the alist
`shell-environment--alist'."
  (let ((val (shell-enivonment-getenv var))
	(var-name
	 (if (stringp var)
	     var
	   (if (symbolp var)
	       (replace-regexp-in-string "^:" ""
					 (symbol-name var))
	     (error "Parameter must be a STRING or a SYMBOL.")))))
    (when val
      (setenv var-name val))))

;;;###autoload
(defun shell-environment-import-all ()
  "Imports/sets all the values of variables on `shell-environment--alist'"
  (unless shell-environment--alist
    (shell-environment-init))
  (length
   (mapc (lambda (e) (setenv (car e) (cdr e)))
	 shell-environment--alist)))

(provide 'shell-environment)

;; Local Variables:
;; indent-tabs-mode: nil
;; End:

;;; shell-environment.el ends here
